%# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
%# Use of this source code is governed by a BSD-style license that can be
%# found in the LICENSE file.
<div class="header">
<form name="frmRefresh" style="margin: 0; padding: 0">
  <a href="/">Home</a> |
  <a href="/logs">Logs</a> |
  <a href="http://cautotest/results/dashboard"
     target="_blank">Dashboard</a> |
  <a href="http://cautotest" target="_blank">CAutotest</a> |
  <a href="http://chromegw.corp.google.com/i/chromiumos/console"
     target="_blank">External buildbot</a> |
  <a href="http://chromegw/i/chromeos/console"
     target="_blank">Internal buildbot</a> |
  <a href="/regenerate">Regenerate</a> |
  <a href="/help">Help</a> |
  <input type="checkbox" name="CB1" onclick="StartTime()">
  <label for="CB1">Auto-refresh (60s)</label>
  <input type="checkbox" name="CB2">
  <label for="CB2">Regenerate on auto-refresh</label>
</form>
</div>
